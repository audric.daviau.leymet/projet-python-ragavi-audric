import warnings
warnings.filterwarnings('ignore')
from bs4 import BeautifulSoup
import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import requests
import csv
from dash.dependencies import Input, Output
import wikipediaapi
import pandas as pd
from plotly.subplots import make_subplots
import plotly.graph_objects as go

# Cette fonction permet de récupérer et stocker dans un fichier csv, les coordonnées gps de tous les pays du monde selon google
def recup_pays(url, name_csv):
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')

    table = soup.find('table')
    df=pd.read_html(str(table))
    df=pd.DataFrame(df[0])
    df.to_csv(name_csv, index=False)

# Cette fonction récupère le tableau des médailles de la page wikipédia qui est passé en paramètre et retourne le tableau dans un csv
# De plus, cette fonction retroune un data file, c'est à dire le tableau en texte pour python
def recup_tableau_medailles(url,name_csv):
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')

    table_medal = soup.find('table', {'class': 'wikitable sortable plainrowheaders jquery-tablesorter'})
    df=pd.read_html(str(table_medal))
    df=pd.DataFrame(df[0])
    df["latitude"] = ""
    df["longitude"] = ""
    df.loc[2, 'Team'] = "Japan"
    df.to_csv(name_csv, index=False)
    return df

# La fonction fusion_pays_coordonnees permet de fusionner les coordonnées des pays participant aux JO en précisant leurs coorodonnées
def fusion_pays_coordonnees(df,name_csv, country):
    with open('./data/country_coord.csv') as file:
        reader = csv.reader(file)
        i = 0
        for c in country:
            file.seek(0)
            j = 0
            for row in reader:
                if row[3] == c:
                    df.loc[i, 'latitude'] = row[1]
                    df.loc[i, 'longitude'] = row[2]
                    df.to_csv(name_csv, index=False)
                    i += 1
                    j = 1
                    break
            if j == 0:
                df.loc[i, 'latitude'] = ""
                df.loc[i, 'longitude'] = ""
                df.to_csv(name_csv, index=False)
                i += 1


info_pays = 'https://developers.google.com/public-data/docs/canonical/countries_csv'
recup_pays(info_pays, "./data/country_coord.csv")

wiki_wiki = wikipediaapi.Wikipedia('en')
url_jo_2020 = wiki_wiki.page('2020_Summer_Olympics_medal_table').fullurl
df = recup_tableau_medailles(url_jo_2020,"./data/tables_olymp.csv")

longitude = []
latitude = []
with open('./data/country_coord.csv') as file:
    reader = csv.reader(file)
    line_count = 0
    for row in reader:
        if line_count >= 1:
            latitude.append(row[1])
            longitude.append(row[2])
        line_count += 1

# récupération des pays qui ont été présent au JO de l'année X
country = []
with open('./data/tables_olymp.csv') as file:
    reader = csv.reader(file)
    line_count = 0
    for row in reader:
        if line_count >= 1:
            country.append(row[1])
        line_count += 1

fusion_pays_coordonnees(df,"./data/tables_olymp.csv",country)

wiki_url2 = 'https://en.wikipedia.org/wiki/France_at_the_2020_Summer_Olympics'

response2 = requests.get(wiki_url2)
soup2 = BeautifulSoup(response2.text, 'html.parser')

table_medal_pays = soup2.find('table', attrs={'style':'font-size:85%; text-align:center;'})
df2=pd.read_html(str(table_medal_pays))
df2=pd.DataFrame(df2[0])
df2.to_csv(r'./data/tables_olymp_country.csv', index=False)

# ouvrir le CSV et le décomposer en plusieurs
sport_name = []
total_medals = []
with open('./data/tables_olymp_country.csv') as file:
    reader = csv.reader(file)
    line_count = 0
    for row in reader:
        if line_count >= 2 and line_count < 18:
            sport_name.append(row[0])
            total_medals.append(row[4])
        line_count += 1
sport_name.append(" ")
total_medals.append(0)

# récupérer le nombre de médailles d'or argent et bronze par pays
med_or = []
med_argent = []
med_bronze = []
with open('./data/tables_olymp.csv') as file:
    reader = csv.reader(file)
    line_count = 0
    for row in reader:
        if line_count >= 1 and line_count < 94:
            med_or.append(row[2])
            med_argent.append(row[3])
            med_bronze.append(row[4])
        line_count += 1

app = dash.Dash(__name__)

colors = {
    'background': '#111111',
    'text': '#7FDBFF'
}
# Initialize figure with subplots
fig = make_subplots(
    rows=2, cols=2,shared_xaxes=True,shared_yaxes=True,
    specs=[[{"colspan": 2},None],
           [{"type": "scattergeo"}, {"type":"pie"}]],
    subplot_titles=("Histogramme représentant les médailles totales en France en 2020","Carte représentant les médailles totales dans chaque pays","Camembert représentant la part de médailles d'or, d'argent et de bronze en France")
)
# camembert 
colors = ['gold','silver','rgb(	205, 127, 50)']
fig.add_trace(go.Bar(name='Médailles totales', x=sport_name, y=total_medals),1, 1).update_layout(yaxis={'categoryorder':'category ascending'})
labels = ['Or','Argent','Bronze']
fig.add_trace(go.Pie(values=[med_or[7], med_argent[7], med_bronze[7]], labels = labels, marker_colors = colors),
              row=2, col=2)

# mapping
df['text'] = df['Team'] + '<br>Total ' + (df['Total']).astype(str)
limits = [(0,2),(3,10),(11,20),(21,50),(50,3000)]
colors = ["royalblue","crimson","lightseagreen","orange","lightgrey"]
cities = []
scale = 4
for i in range(len(limits)):
    lim = limits[i]
    df_sub = df[lim[0]:lim[1]]
    fig.add_trace(go.Scattergeo(
        locationmode = 'ISO-3',
        lon = df_sub['longitude'],
        lat = df_sub['latitude'],
        text = df_sub['text'],
        marker = dict(
            size = df_sub['Total'] * scale,
            color = colors[i],
            line_color='rgb(40,40,40)',
            line_width=0.5,
            sizemode = 'area'
        ),
        name = '{0} - {1}'.format(lim[0],lim[1]))).update_geos(projection_type="natural earth")
fig.update_layout(height=900, showlegend=False)

app.layout = html.Div( children=[
    
    html.H1(
        children='Jeux Olympique d\'été 2020',
        style={
            'textAlign': 'center',
            'color': 'black',
            'border':'thick double #32a1ce'
        }
    ),

    html.H2(
        children='Focus sur la France',
        style={
            'textAlign': 'center',
            'color': 'blue'
        }
    ),

    dcc.Graph(
        id='example-graph-2',
        config={'scrollZoom': True},
        figure=fig
    )
])


if __name__ == '__main__':
    app.run_server(debug=True)
