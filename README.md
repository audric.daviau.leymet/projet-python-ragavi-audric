# Projet 
Voici le projet Python du premier semestre de Ragavi SAGEEKARAN et Audric DAVIAU-LEYMET.

## Description
Le projet se concentre sur les Jeux Olympique d'été 2020. A partir de la page Wikipédia sur celle-ci.
Notre projet consiste à faire des statistiques comme les couleurs des médailles en fonction du pays, les sports, les pays...

## Lancement
Pour lancer notre projet, il faut commencer par cloner notre dépôt git. Pour ce faire, lancer un terminal si vous êtes sous linux ou bien git bash sous windows puis suivez les commandes ci-dessous.
Si vous n'avez pas git bash, vous pouvez le télécharger ici : https://gitforwindows.org/
```shell
cd Documents
mkdir projet_JO
cd projet_JO/
git clone git@gitlab.com:audric.daviau.leymet/projet-python-ragavi-audric.git
```

Maintenant que le projet a été cloné, nous aller télécharger toutes les dépendances. Celles-ci se trouvent dans le fichier requirements.txt. Mais ne vous embêtez pas à toutes les installer manuellement, tapez la commande ci-dessous dans votre terminal :
```shell
python -m pip install -r requirements.txt
```
## Démarrage
Maintenant, vous pouvez lancer le projet ! Pour le lancer copiez dans votre terminal la commande suivante
```shell
python DashBoard.py
```

Cliquez sur le lien http://127.0.0.1:8050/ qui s'est générer dans le terminal. Vous tomberez sur le dashbord

